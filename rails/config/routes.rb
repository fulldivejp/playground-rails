Rails.application.routes.draw do
  devise_for :admins
  
  root 'job_offers#index'

  authenticate :admin do
    namespace :admin do
      root :to => "job_offers#index"
      resources :job_offers
      resources :job_titles
      resources :companies
      resources :countries
      resources :cities
      resources :technologies
      resources :salary_periods
      resources :currencies
      resources :employment_types
      resources :work_types
    end
  end

  resources :job_offers, only: [:index, :show] do
    collection do
      get :search, :action => 'search_redirect', :as => 'search'
      get 'sae/:search_what/:search_where', :action => 'search', :constraints => { :search_what => /[^\/]+/ }, :as => 'search_what_where'
      get 'sa/:search_what', :action => 'search', :constraints => { :search_what => /[^\/]+/ }, :as => 'search_what'
      get 'se/:search_where', :action => 'search', :constraints => { :search_what => /[^\/]+/ }, :as => 'search_where'
    end
  end 
end
