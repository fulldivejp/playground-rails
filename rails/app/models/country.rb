class Country < ActiveRecord::Base
  has_many :job_offers
  has_many :cities

  def self.tokens(query)
    countries = where("lower(name) like ?", "%#{query.downcase}%")
    if countries.empty?
      [{id: "<<<#{query}>>>", name: "New: \"#{query}\""}]
    else
      countries.unshift({id: "<<<#{query}>>>", name: "New: \"#{query}\""})
    end
  end

  def self.id_from_token(token)
    token.gsub!(/<<<(.+?)>>>/) { create!(name: $1).id }
    token
  end
end
