class City < ActiveRecord::Base
  has_many :job_offers
  belongs_to :country

  def self.tokens(query)
    cities = where("lower(name) like ?", "%#{query.downcase}%")
    if cities.empty?
      [{id: "<<<#{query}>>>", name: "New: \"#{query}\""}]
    else
      cities.unshift({id: "<<<#{query}>>>", name: "New: \"#{query}\""})
    end
  end

  def self.id_from_token(token)
    token.gsub!(/<<<(.+?)>>>/) { create!(name: $1).id }
    token
  end
end
