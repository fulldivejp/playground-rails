class JobTitle < ActiveRecord::Base
  has_many :job_offers

  def self.tokens(query)
    job_titles = where("lower(name) like ?", "%#{query.downcase}%")
    if job_titles.empty?
      [{id: "<<<#{query}>>>", name: "New: \"#{query}\""}]
    else
      job_titles.unshift({id: "<<<#{query}>>>", name: "New: \"#{query}\""})
    end
  end

  def self.id_from_token(token)
    token.gsub!(/<<<(.+?)>>>/) { create!(name: $1).id }
    token
  end
end
