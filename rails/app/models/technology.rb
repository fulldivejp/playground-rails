class Technology < ActiveRecord::Base
  has_and_belongs_to_many :job_offers

  def self.tokens(query)
    technologies = where("lower(name) like ?", "%#{query.downcase}%")
    if technologies.empty?
      [{id: "<<<#{query}>>>", name: "New: \"#{query}\""}]
    else
      technologies.unshift({id: "<<<#{query}>>>", name: "New: \"#{query}\""})
    end
  end

  def self.ids_from_tokens(tokens)
    tokens.gsub!(/<<<(.+?)>>>/) { create!(name: $1).id }
    tokens.split(',')
  end
end
