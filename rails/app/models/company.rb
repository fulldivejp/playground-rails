class Company < ActiveRecord::Base
  has_many :job_offers

  def self.tokens(query)
    companies = where("lower(name) like ?", "%#{query.downcase}%")
    if companies.empty?
      [{id: "<<<#{query}>>>", name: "New: \"#{query}\""}]
    else
      companies.unshift({id: "<<<#{query}>>>", name: "New: \"#{query}\""})
    end
  end

  def self.id_from_token(token)
    token.gsub!(/<<<(.+?)>>>/) { create!(name: $1).id }
    token
  end
end
