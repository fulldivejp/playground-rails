class JobOffer < ActiveRecord::Base
  include PgSearch
  pg_search_scope :search_what,
    against: [:title, :description],
    associated_against: {
      job_title: :name,
      technologies: :name,
      company: :name
    },
    :using => {
      :tsearch => {:any_word => true, :dictionary => "english"}
    }

  pg_search_scope :search_where,
    associated_against: {
      country: [:name, :code],
      city: :name
    },
    :using => {
      :tsearch => {:any_word => true, :dictionary => "english"}
    }

  default_scope { order(:created_at => :desc) }
  self.per_page = 25

  belongs_to :company
  belongs_to :job_title
  belongs_to :country
  belongs_to :city
  has_and_belongs_to_many :technologies
  belongs_to :currency
  belongs_to :salary_period
  belongs_to :employment_type
  belongs_to :work_type

  def to_param
    [id, title.parameterize].join("-")
  end

  attr_reader :job_title_token
  attr_reader :company_token
  attr_reader :country_token
  attr_reader :city_token
  attr_reader :technology_tokens

  def job_title_token=(token)
    self.job_title_id = JobTitle.id_from_token(token)
  end

  def company_token=(token)
    self.company_id = Company.id_from_token(token)
  end

  def country_token=(token)
    self.country_id = Country.id_from_token(token)
  end

  def city_token=(token)
    self.city_id = City.id_from_token(token)
  end

  def technology_tokens=(tokens)
    self.technology_ids = Technology.ids_from_tokens(tokens)
  end

end
