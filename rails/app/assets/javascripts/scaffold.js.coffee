
$(document).on \
  'turbolinks:click', \
  -> 
    $(".loading-indicator").show()
    console.log('loading page')

$(document).on \
  'turbolinks:load', \
  -> 
    $(".loading-indicator").hide()
    console.log('page loaded')

$(document).on \
  "turbolinks:load", \
  ->
    # mobile menu button
    $(".button-collapse").sideNav();

    # menu dropdown
    $(".dropdown-button").dropdown();

    # enable select
    $('select').material_select();

    # refresh materialize input fileds (label overlay issue)
    Materialize.updateTextFields()

    # fixing materialize textarea height problem with turbolink
    $('textarea').each ->
      # Calculate number of lines
      numberOfLines = $(this).val().split(/\r\n|\r|\n/).length
      # Get lineheight from CSS
      lineHeight = $(this).css('line-height')
      # Convert from px-string to number
      lineHeight = lineHeight.replace("px", "")
      # Calculate height for textarea
      textAreaHeight = numberOfLines*lineHeight
      # Set minimal height to 300px
      textAreaHeight = if textAreaHeight > 300 then textAreaHeight else 300
      # Convert from number to px-string
      stylingTextAreaHeight = "".concat(textAreaHeight).concat("px")
      # Set textarea height to calculated height
      $(this).css("height",stylingTextAreaHeight)

    # enable chosen js
    console.log('chosen loaded')
    $('.chosen-select').chosen
      allow_single_deselect: true
      no_results_text: 'No results matched'
      width: '300px'