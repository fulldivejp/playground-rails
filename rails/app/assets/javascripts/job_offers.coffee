$(document).on \
  "turbolinks:load", \
  ->
    # enable tokeninput for job title field
    $('.job-offers-page__job-title-field').tokenInput \
      '/admin/job_titles.json', \
      prePopulate: $('.job-offers-page__job-title-field').data('load'), \
      tokenLimit: 1

    # enable tokeninput for company field
    $('.job-offers-page__company-field').tokenInput \
      '/admin/companies.json', \
      prePopulate: $('.job-offers-page__company-field').data('load'), \
      tokenLimit: 1

    # enable tokeninput for country field
    $('.job-offers-page__country-field').tokenInput \
      '/admin/countries.json', \
      prePopulate: $('.job-offers-page__country-field').data('load'), \
      tokenLimit: 1

    # enable tokeninput for city field
    $('.job-offers-page__city-field').tokenInput \
      '/admin/cities.json', \
      prePopulate: $('.job-offers-page__city-field').data('load'), \
      tokenLimit: 1

    # enable tokeninput for technologies field
    $('.job-offers-page__technologies-field').tokenInput \
      '/admin/technologies.json', \
      prePopulate: $('.job-offers-page__technologies-field').data('load'), \
      preventDuplicates: true, \
      tokenLimit: null

    # active_to datepicker
    $('.datepicker').pickadate()