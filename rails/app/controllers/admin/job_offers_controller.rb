class Admin::JobOffersController < ApplicationController
  before_action :set_job_offer, only: [:show, :edit, :update, :destroy]

  layout 'admin'

  def index
    @job_offers = JobOffer.all
  end

  def show
  end

  def new
    @job_offer = JobOffer.new
    @job_offer.active_to = 45.days.from_now
  end

  def edit
  end

  def create
    @job_offer = JobOffer.new(job_offer_params)

    respond_to do |format|
      if @job_offer.save
        format.html { redirect_to admin_job_offer_path(@job_offer.id), notice: 'Job offer was successfully created.' }
        format.json { render :show, status: :created, location: @job_offer }
      else
        format.html { render :new }
        format.json { render json: @job_offer.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @job_offer.update(job_offer_params)
        format.html { redirect_to admin_job_offer_path(@job_offer), notice: 'Job offer was successfully updated.' }
        format.json { render :show, status: :ok, location: @job_offer }
      else
        format.html { render :edit }
        format.json { render json: @job_offer.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @job_offer.destroy
    respond_to do |format|
      format.html { redirect_to admin_job_offers_path, notice: 'Job offer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job_offer
      @job_offer = JobOffer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def job_offer_params
      params.require(:job_offer).permit(
        :title,
        :job_title_token,
        :link,
        :company_token,
        :description,
        :country_token,
        :city_token,
        :address,
        :company_name,
        :technology_tokens,
        :is_active,
        :active_to,
        :salary_from,
        :salary_to,
        :currency_id,
        :salary_period_id,
        :employment_type_id,
        :work_type_id
      )
    end
end
