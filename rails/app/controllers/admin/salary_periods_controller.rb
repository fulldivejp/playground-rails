class Admin::SalaryPeriodsController < ApplicationController
  before_action :set_salary_period, only: [:show, :edit, :update, :destroy]

  layout 'admin'

  def index
    @salary_periods = SalaryPeriod.all
  end

  def show
  end

  def new
    @salary_period = SalaryPeriod.new
  end

  def edit
  end

  def create
    @salary_period = SalaryPeriod.new(salary_period_params)

    respond_to do |format|
      if @salary_period.save
        format.html { redirect_to admin_salary_period_path(@salary_period), notice: 'Salary period was successfully created.' }
        format.json { render :show, status: :created, location: @salary_period }
      else
        format.html { render :new }
        format.json { render json: @salary_period.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @salary_period.update(salary_period_params)
        format.html { redirect_to admin_salary_period_path(@salary_period), notice: 'Salary period was successfully updated.' }
        format.json { render :show, status: :ok, location: @salary_period }
      else
        format.html { render :edit }
        format.json { render json: @salary_period.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @salary_period.destroy
    respond_to do |format|
      format.html { redirect_to admin_salary_periods_path, notice: 'Salary period was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_salary_period
      @salary_period = SalaryPeriod.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def salary_period_params
      params.require(:salary_period).permit(:period, :duration_code)
    end
end
