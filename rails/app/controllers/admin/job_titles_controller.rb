class Admin::JobTitlesController < ApplicationController
  before_action :set_job_title, only: [:show, :edit, :update, :destroy]

  layout 'admin'

  def index
    @job_titles = JobTitle.order(:name)
    respond_to do |format|
      format.html
      format.json { render json: @job_titles.tokens(params[:q]) }
    end
  end

  def show
  end

  def new
    @job_title = JobTitle.new
  end

  def edit
  end

  def create
    @job_title = JobTitle.new(job_title_params)

    respond_to do |format|
      if @job_title.save
        format.html { redirect_to admin_job_title_path(@job_title), notice: 'Job title was successfully created.' }
        format.json { render :show, status: :created, location: @job_title }
      else
        format.html { render :new }
        format.json { render json: @job_title.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @job_title.update(job_title_params)
        format.html { redirect_to admin_job_title_path(@job_title), notice: 'Job title was successfully updated.' }
        format.json { render :show, status: :ok, location: @job_title }
      else
        format.html { render :edit }
        format.json { render json: @job_title.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @job_title.destroy
    respond_to do |format|
      format.html { redirect_to admin_job_titles_path, notice: 'Job title was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job_title
      @job_title = JobTitle.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def job_title_params
      params.require(:job_title).permit(:name)
    end
end
