class JobOffersController < ApplicationController
  before_action :set_job_offer, only: [:show]

  def index
    @job_offers = JobOffer
      .where(is_active: true)
      .where("active_to >= :now", now: DateTime.now)
      .page(params[:page])
  end

  def show
  end

  # SEARCHNIG ==========
  def search_redirect
    # this function is called when you are using search form - it redirects to more nice and seo friendly path
    if params[:search_what].present? && params[:search_where].present?
      redirect_to search_what_where_job_offers_path(params[:search_what], params[:search_where])
    elsif params[:search_what].present?
      redirect_to search_what_job_offers_path(params[:search_what])
    elsif params[:search_where].present?
      redirect_to search_where_job_offers_path(params[:search_where])
    else
      redirect_to job_offers_path
    end
  end

  def search
    # search is based on two fields so that's why there is so much stuff here
    if params[:search_what].present? && params[:search_where].present?
      @what = params[:search_what].gsub(/-/, ' ').titleize
      @where = params[:search_where].gsub(/-/, ' ').titleize
      @search_query = @what + ' jobs in ' + @where

      @job_offers = JobOffer
        .where(is_active: true)
        .where("active_to >= :now", now: DateTime.now)
        .search_what(@what)
        .search_where(@where)
        .page(params[:page])

    elsif params[:search_what].present?
      @what = params[:search_what].gsub(/-/, ' ').titleize
      @search_query = @what + ' jobs'

      @job_offers = JobOffer
        .where(is_active: true)
        .where("active_to >= :now", now: DateTime.now)
        .search_what(@what)
        .page(params[:page])

    elsif params[:search_where].present?
      @where = params[:search_where].gsub(/-/, ' ').titleize
      @search_query = 'IT jobs in ' + @where
      
      @job_offers = JobOffer
        .where(is_active: true)
        .where("active_to >= :now", now: DateTime.now)
        .search_where(@where)
        .page(params[:page])

    else
      redirect_to job_offers_path
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job_offer
      @job_offer = JobOffer.find(params[:id])

      unless @job_offer.active_to >= DateTime.now && @job_offer.is_active
        flash[:error] ||= []
        flash[:error] << 'Oops! This job offer is no longer active. Check out our latest ones.'
        redirect_to job_offers_path
      end
    end
end
