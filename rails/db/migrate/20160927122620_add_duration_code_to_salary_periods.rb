class AddDurationCodeToSalaryPeriods < ActiveRecord::Migration
  def change
    add_column :salary_periods, :duration_code, :string
  end
end
