class AddEmploymentTypeToJobOffers < ActiveRecord::Migration
  def change
    add_reference :job_offers, :employment_type, index: true, foreign_key: true
  end
end
