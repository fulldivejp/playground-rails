class AddSalaryFromToJobOffers < ActiveRecord::Migration
  def change
    add_column :job_offers, :salary_from, :integer
  end
end
