class AddCurrencyToJobOffers < ActiveRecord::Migration
  def change
    add_reference :job_offers, :currency, index: true, foreign_key: true
  end
end
