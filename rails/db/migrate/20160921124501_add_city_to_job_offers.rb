class AddCityToJobOffers < ActiveRecord::Migration
  def change
    add_reference :job_offers, :city, index: true, foreign_key: true
  end
end
