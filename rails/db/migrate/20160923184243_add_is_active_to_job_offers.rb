class AddIsActiveToJobOffers < ActiveRecord::Migration
  def change
    add_column :job_offers, :is_active, :boolean
  end
end
