class AddSalaryToToJobOffers < ActiveRecord::Migration
  def change
    add_column :job_offers, :salary_to, :integer
  end
end
