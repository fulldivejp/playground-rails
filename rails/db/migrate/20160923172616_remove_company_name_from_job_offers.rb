class RemoveCompanyNameFromJobOffers < ActiveRecord::Migration
  def change
    remove_column :job_offers, :company_name, :string
  end
end
