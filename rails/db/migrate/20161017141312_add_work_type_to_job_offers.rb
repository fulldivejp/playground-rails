class AddWorkTypeToJobOffers < ActiveRecord::Migration
  def change
    add_reference :job_offers, :work_type, index: true, foreign_key: true
  end
end
