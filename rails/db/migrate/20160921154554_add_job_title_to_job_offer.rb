class AddJobTitleToJobOffer < ActiveRecord::Migration
  def change
    add_reference :job_offers, :job_title, index: true, foreign_key: true
  end
end
