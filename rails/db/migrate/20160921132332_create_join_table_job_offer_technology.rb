class CreateJoinTableJobOfferTechnology < ActiveRecord::Migration
  def change
    create_join_table :job_offers, :technologies do |t|
      # t.index [:job_offer_id, :technology_id]
      # t.index [:technology_id, :job_offer_id]
    end
  end
end
