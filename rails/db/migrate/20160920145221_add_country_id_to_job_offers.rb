class AddCountryIdToJobOffers < ActiveRecord::Migration
  def change
    add_column :job_offers, :country_id, :integer
  end
end
