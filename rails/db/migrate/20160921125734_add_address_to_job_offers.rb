class AddAddressToJobOffers < ActiveRecord::Migration
  def change
    add_column :job_offers, :address, :text
  end
end
