class AddSalaryPeriodToJobOffers < ActiveRecord::Migration
  def change
    add_reference :job_offers, :salary_period, index: true, foreign_key: true
  end
end
