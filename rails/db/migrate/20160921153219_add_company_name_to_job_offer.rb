class AddCompanyNameToJobOffer < ActiveRecord::Migration
  def change
    add_column :job_offers, :company_name, :string
  end
end
