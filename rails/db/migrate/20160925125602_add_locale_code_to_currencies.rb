class AddLocaleCodeToCurrencies < ActiveRecord::Migration
  def change
    add_column :currencies, :locale_code, :string
  end
end
