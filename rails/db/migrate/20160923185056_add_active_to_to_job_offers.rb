class AddActiveToToJobOffers < ActiveRecord::Migration
  def change
    add_column :job_offers, :active_to, :datetime
  end
end
