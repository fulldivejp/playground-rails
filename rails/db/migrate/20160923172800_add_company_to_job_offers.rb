class AddCompanyToJobOffers < ActiveRecord::Migration
  def change
    add_reference :job_offers, :company, index: true, foreign_key: true
  end
end
