class CreateSalaryPeriods < ActiveRecord::Migration
  def change
    create_table :salary_periods do |t|
      t.string :period

      t.timestamps null: false
    end
  end
end
