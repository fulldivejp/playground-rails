# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161017141312) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string   "email",               default: "", null: false
    t.string   "encrypted_password",  default: "", null: false
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",       default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree

  create_table "cities", force: :cascade do |t|
    t.string   "name"
    t.integer  "country_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "countries", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "code"
  end

  create_table "currencies", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.string   "sign"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "locale_code"
  end

  create_table "employment_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "job_offers", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.string   "link"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "country_id"
    t.integer  "city_id"
    t.text     "address"
    t.integer  "job_title_id"
    t.integer  "company_id"
    t.boolean  "is_active"
    t.datetime "active_to"
    t.integer  "salary_from"
    t.integer  "salary_to"
    t.integer  "currency_id"
    t.integer  "salary_period_id"
    t.integer  "employment_type_id"
    t.integer  "work_type_id"
  end

  add_index "job_offers", ["city_id"], name: "index_job_offers_on_city_id", using: :btree
  add_index "job_offers", ["company_id"], name: "index_job_offers_on_company_id", using: :btree
  add_index "job_offers", ["currency_id"], name: "index_job_offers_on_currency_id", using: :btree
  add_index "job_offers", ["employment_type_id"], name: "index_job_offers_on_employment_type_id", using: :btree
  add_index "job_offers", ["job_title_id"], name: "index_job_offers_on_job_title_id", using: :btree
  add_index "job_offers", ["salary_period_id"], name: "index_job_offers_on_salary_period_id", using: :btree
  add_index "job_offers", ["work_type_id"], name: "index_job_offers_on_work_type_id", using: :btree

  create_table "job_offers_technologies", id: false, force: :cascade do |t|
    t.integer "job_offer_id",  null: false
    t.integer "technology_id", null: false
  end

  create_table "job_titles", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "salary_periods", force: :cascade do |t|
    t.string   "period"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "duration_code"
  end

  create_table "technologies", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "work_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "job_offers", "cities"
  add_foreign_key "job_offers", "companies"
  add_foreign_key "job_offers", "currencies"
  add_foreign_key "job_offers", "employment_types"
  add_foreign_key "job_offers", "job_titles"
  add_foreign_key "job_offers", "salary_periods"
  add_foreign_key "job_offers", "work_types"
end
