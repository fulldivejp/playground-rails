require 'test_helper'

class SalaryPeriodsControllerTest < ActionController::TestCase
  setup do
    @salary_period = salary_periods(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:salary_periods)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create salary_period" do
    assert_difference('SalaryPeriod.count') do
      post :create, salary_period: { period: @salary_period.period }
    end

    assert_redirected_to salary_period_path(assigns(:salary_period))
  end

  test "should show salary_period" do
    get :show, id: @salary_period
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @salary_period
    assert_response :success
  end

  test "should update salary_period" do
    patch :update, id: @salary_period, salary_period: { period: @salary_period.period }
    assert_redirected_to salary_period_path(assigns(:salary_period))
  end

  test "should destroy salary_period" do
    assert_difference('SalaryPeriod.count', -1) do
      delete :destroy, id: @salary_period
    end

    assert_redirected_to salary_periods_path
  end
end
